# prérequis
* installer la dernière version d'[OBS](https://obsproject.com/)
* installer le plugin [move-transition](https://obsproject.com/forum/resources/move-transition.913/)
* installer [NDI-Tools](https://www.ndi.tv/tools/) et plugin [NDI](https://obsproject.com/forum/resources/obs-ndi-newtek-ndi%E2%84%A2-integration-into-obs-studio.528/)

ATTENTION : le profile actuel est prévu et compatible pour des PC sous Windows. Sous MAC il est nécesssaire de faire quelques adaptations avant import
# Import dans OBS

* Télécharger l'archive ([lien direct](https://gitlab.com/obs-plugin/obs-alternatiba-paris/-/archive/main/obs-alternatiba-paris-main.zip))
* Pour que les chemins vers les ressources (images, vidéos, logos, ...) soient valident dès l'imprt, deux solutions :
   * **Solution 1 :** décompresser le fichier de sorte à retrouver le contenu de l'archive sous le dossier "C:/OBS/obs-alternatiba-paris/
   * **Solution 2 :** modifier (rechercher/remplacer) le chemin "C:/OBS/obs-alternatiba-paris/ dans le fichier MAIN_PARAM.json pour le remplacer par le chemin local où vous l'avez décompressé. (bien chercher et remplacer par des / et non des \

# Principe général de fonctionnement

Documentation partagée [ à compléter ](https://docs.google.com/presentation/d/1i2Ik4fNI9JwR13Z8wLucg6x9J7BpnU6NrCfKDWOHuEQ/edit?usp=sharing)

**scn_main** est le point d'entrée du profil. Dans cette scène, il sera est possible de choisir 3 modes de fonctionnement et de dispoisition :
* mono : adapté aux conférences en physique avec plusieurs sources passant de l'une à l'autre avec les touches F1...F3
* multi : adapté aux conférences zoom pures de 1 intervenant à 12 intervenants
* hybride

elmt_01 à elmt_13 sont une liste de scènes intermédiaires servant à séparer les dispositions préenregistrées mono/multi/hybride des ces éléments et les sources qui sont dédiées à ces mêmes éléments. Chaque élément peut être paramétré pour afficher :
* une des 3 sources vidéos input_vidéo_01 à 03
* un des 2 médias préconfigurés input_media_01 et 02
* un des 12 intervenants de la fenêtre zoom crop_zoom_01 à 12
* la scène d'intro de la conférence scn_intro (pour combler un trou permettant d'avoir un nomber impair d'éléments en mode multi par ex)


***elmt_pip*** Elément affichable de type Picture-In-Picture généralement affiché au-dessus d'un élément plus global sur une petite zone de l'image. S'affiche et se cache grâce au raccourcis CTRL+TABULATION
